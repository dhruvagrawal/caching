package com.sap.cloud.sdk.tutorial;

import com.google.gson.Gson;
import org.slf4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.*;
import org.springframework.beans.factory.annotation.Autowired;

import redis.clients.jedis.Jedis;
import java.lang.*;

import java.util.*;

@WebServlet("/costcenters")
public class CostCenterServlet extends HttpServlet {
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final long serialVersionUID = 1L;
	//private static final Logger logger = CloudLoggerFactory.getLogger(CostCenterServlet.class);
	
	
//	public Jedis jedis() {
//        public Jedis jedis = new Jedis("10.11.241.31", 48927);
//        jedis.auth("Li_Xf19_mp4cHeOY");
//        return jedis;
//    }
//	
//	@Autowired
//	public Jedis jedis;
	
	@Override
	protected void doGet( final HttpServletRequest request, final HttpServletResponse response )
			throws ServletException,
			IOException
	{
		Jedis jedis = new Jedis("10.11.241.31", 48927);
        jedis.auth("Li_Xf19_mp4cHeOY");
		try {
			/* final ErpEndpoint endpoint = new ErpEndpoint();
            final List<NorthWindData> costCenters = ODataQueryBuilder
                    .withEntity("/V4/Northwind/Northwind.svc/", "Categories")
                    .select("CategoryID", "CategoryName", "Description")
                    .build()
                    .execute(endpoint)
                    .asList(NorthWindData.class);*/
			if (!jedis.exists("northwindData")) {
			URL urlobj = new URL("http://services.odata.org/V2/Northwind/Northwind.svc/Categories?$format=json");

			//URL obj = new URL(GET_URL);
			HttpURLConnection con = (HttpURLConnection) urlobj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			int responseCode = con.getResponseCode();
			System.out.println("GET Response Code :: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				StringBuffer responseData = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					responseData.append(inputLine);
				}
				in.close();

				
//				JSONObject objData = new JSONObject(responseData.toString());
				
				
				//jedis.del("northwindData");
			    //log.info("Setting Categories : ");
			    //java.util.ArrayList arrayData = (obj33.get("d")).get("results");
			    
			    
		       // for (JSONObject obj : arrayData) {
		            jedis.rpush("northwindData", responseData.toString());
		    //    }
		        //log.info(jedis.lrange("northwindData", 0, -1).toString());
				
				
		        response.setContentType("application/json");
				response.getWriter().write(responseData.toString());
			} else {
				System.out.println("GET request not worked");
			}
			}
			else{
				response.getWriter().write("Cached Block:" +readAll());
			}
			

		} catch(final Exception e) {
		//logger.error(e.getMessage(), e);
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		response.getWriter().write(e.getMessage());
	}
}
public String readAll() throws Exception{
	 Jedis jedis = new Jedis("10.11.241.31", 48927);
     jedis.auth("Li_Xf19_mp4cHeOY");
     List<String> data = null;
	   if (jedis.exists("northwindData")) {
		    data = jedis.lrange("northwindData", 0, -1);
		    
		}
	   return data.get(0);
	}
}